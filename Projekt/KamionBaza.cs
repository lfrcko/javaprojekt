﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Collections;

namespace Projekt
{
    public class KamionBaza
    {
        private ArrayList bazaKamiona;

        public KamionBaza()
        {
            bazaKamiona = new ArrayList();
            Deserialize();
        }

        public void DodajKamion(Kamion kamion)
        {
            bazaKamiona.Add(kamion);
            Serialize();
        }

        public ArrayList DohvatiKamione()
        {
            return bazaKamiona;
        }

        public void Serialize()
        {
            try
            {
                string kamioniInJson = JsonSerializer.Serialize(bazaKamiona);
                using (StreamWriter KamioniUnos = new(@"D:\Projekt\Projekt\Kamioni.txt"))
                {
                    KamioniUnos.WriteLine(kamioniInJson);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nema podataka");
                Console.WriteLine(ex.Message);
            }
        }

        public void Deserialize()
        {
            try
            {
                string json = File.ReadAllText(@"D:\Projekt\Projekt\Kamioni.txt");
                List<Kamion> ListaKamiona = JsonSerializer.Deserialize<List<Kamion>>(json)!;
                foreach (Kamion k in ListaKamiona)
                {
                    bazaKamiona.Add(k);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
