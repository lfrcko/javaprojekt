﻿namespace Projekt
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnZatvori = new System.Windows.Forms.Button();
            this.txtBoxBrojVrata = new System.Windows.Forms.TextBox();
            this.lblBrojSjedala = new System.Windows.Forms.Label();
            this.btnUnesi = new System.Windows.Forms.Button();
            this.txtBoxBojaAuta = new System.Windows.Forms.TextBox();
            this.txtBoxSnagaAuta = new System.Windows.Forms.TextBox();
            this.txtBoxNazivAuta = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBojaAuta = new System.Windows.Forms.Label();
            this.lblNazivAuta = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnZatvori
            // 
            this.btnZatvori.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnZatvori.Location = new System.Drawing.Point(9, 341);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(213, 33);
            this.btnZatvori.TabIndex = 19;
            this.btnZatvori.Text = "Zatvori porzor";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.buttonZatvori_Click);
            // 
            // txtBoxBrojVrata
            // 
            this.txtBoxBrojVrata.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxBrojVrata.Location = new System.Drawing.Point(9, 252);
            this.txtBoxBrojVrata.Name = "txtBoxBrojVrata";
            this.txtBoxBrojVrata.Size = new System.Drawing.Size(213, 29);
            this.txtBoxBrojVrata.TabIndex = 18;
            // 
            // lblBrojSjedala
            // 
            this.lblBrojSjedala.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBrojSjedala.Location = new System.Drawing.Point(45, 226);
            this.lblBrojSjedala.Name = "lblBrojSjedala";
            this.lblBrojSjedala.Size = new System.Drawing.Size(134, 23);
            this.lblBrojSjedala.TabIndex = 17;
            this.lblBrojSjedala.Text = "Broj vrata";
            this.lblBrojSjedala.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnesi
            // 
            this.btnUnesi.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnUnesi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUnesi.Location = new System.Drawing.Point(9, 287);
            this.btnUnesi.Name = "btnUnesi";
            this.btnUnesi.Size = new System.Drawing.Size(213, 33);
            this.btnUnesi.TabIndex = 16;
            this.btnUnesi.Text = "Unesi";
            this.btnUnesi.UseVisualStyleBackColor = true;
            this.btnUnesi.Click += new System.EventHandler(this.btnUnesi_Click);
            // 
            // txtBoxBojaAuta
            // 
            this.txtBoxBojaAuta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxBojaAuta.Location = new System.Drawing.Point(9, 104);
            this.txtBoxBojaAuta.Name = "txtBoxBojaAuta";
            this.txtBoxBojaAuta.Size = new System.Drawing.Size(213, 29);
            this.txtBoxBojaAuta.TabIndex = 15;
            // 
            // txtBoxSnagaAuta
            // 
            this.txtBoxSnagaAuta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxSnagaAuta.Location = new System.Drawing.Point(9, 179);
            this.txtBoxSnagaAuta.Name = "txtBoxSnagaAuta";
            this.txtBoxSnagaAuta.Size = new System.Drawing.Size(213, 29);
            this.txtBoxSnagaAuta.TabIndex = 14;
            // 
            // txtBoxNazivAuta
            // 
            this.txtBoxNazivAuta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxNazivAuta.Location = new System.Drawing.Point(9, 35);
            this.txtBoxNazivAuta.Name = "txtBoxNazivAuta";
            this.txtBoxNazivAuta.Size = new System.Drawing.Size(213, 29);
            this.txtBoxNazivAuta.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(45, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 23);
            this.label3.TabIndex = 12;
            this.label3.Text = "Snaga auta";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBojaAuta
            // 
            this.lblBojaAuta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBojaAuta.Location = new System.Drawing.Point(45, 78);
            this.lblBojaAuta.Name = "lblBojaAuta";
            this.lblBojaAuta.Size = new System.Drawing.Size(134, 23);
            this.lblBojaAuta.TabIndex = 11;
            this.lblBojaAuta.Text = "Boja auta";
            this.lblBojaAuta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNazivAuta
            // 
            this.lblNazivAuta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNazivAuta.Location = new System.Drawing.Point(45, 9);
            this.lblNazivAuta.Name = "lblNazivAuta";
            this.lblNazivAuta.Size = new System.Drawing.Size(134, 23);
            this.lblNazivAuta.TabIndex = 10;
            this.lblNazivAuta.Text = "Naziv auta";
            this.lblNazivAuta.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(233, 383);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.txtBoxBrojVrata);
            this.Controls.Add(this.lblBrojSjedala);
            this.Controls.Add(this.btnUnesi);
            this.Controls.Add(this.txtBoxBojaAuta);
            this.Controls.Add(this.txtBoxSnagaAuta);
            this.Controls.Add(this.txtBoxNazivAuta);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblBojaAuta);
            this.Controls.Add(this.lblNazivAuta);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btnZatvori;
        private TextBox txtBoxBrojVrata;
        private Label lblBrojSjedala;
        private Button btnUnesi;
        private TextBox txtBoxBojaAuta;
        private TextBox txtBoxSnagaAuta;
        private TextBox txtBoxNazivAuta;
        private Label label3;
        private Label lblBojaAuta;
        private Label lblNazivAuta;
    }
}