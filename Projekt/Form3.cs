﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt
{
    public partial class Form3 : Form
    {
        public Kamion? NoviKamion { get; set; }
        public Form3()
        {
            InitializeComponent();
        }

        private void btnUnesi_Click(object sender, EventArgs e) {
            NoviKamion = new Kamion(txtBoxNazivKamiona.Text, txtBoxBojaKamiona.Text, int.Parse(txtBoxSnagaKamiona.Text), int.Parse(txtBoxKilometri.Text));
            this.Hide();
        }

        private void btnZatvori_Click(object sender, EventArgs e) {
            this.Hide();
        }
    }
}
