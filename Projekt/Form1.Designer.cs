﻿namespace Projekt
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSecretMenu = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnUnosKamion = new System.Windows.Forms.Button();
            this.btnUnosAuta = new System.Windows.Forms.Button();
            this.lblKamioni = new System.Windows.Forms.Label();
            this.lblAuti = new System.Windows.Forms.Label();
            this.listBoxAuti = new System.Windows.Forms.ListBox();
            this.listBoxKamioni = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // btnSecretMenu
            // 
            this.btnSecretMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSecretMenu.Location = new System.Drawing.Point(474, 424);
            this.btnSecretMenu.Name = "btnSecretMenu";
            this.btnSecretMenu.Size = new System.Drawing.Size(161, 50);
            this.btnSecretMenu.TabIndex = 15;
            this.btnSecretMenu.Text = "Otvori tajni menu";
            this.btnSecretMenu.UseVisualStyleBackColor = true;
            this.btnSecretMenu.Click += new System.EventHandler(this.btnSecretMenu_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(12, 480);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(432, 23);
            this.textBox1.TabIndex = 14;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btnUnosKamion
            // 
            this.btnUnosKamion.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUnosKamion.Location = new System.Drawing.Point(474, 101);
            this.btnUnosKamion.Name = "btnUnosKamion";
            this.btnUnosKamion.Size = new System.Drawing.Size(161, 45);
            this.btnUnosKamion.TabIndex = 13;
            this.btnUnosKamion.Text = "Unesi novi kamion";
            this.btnUnosKamion.UseVisualStyleBackColor = true;
            this.btnUnosKamion.Click += new System.EventHandler(this.btnUnosKamion_Click);
            // 
            // btnUnosAuta
            // 
            this.btnUnosAuta.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUnosAuta.Location = new System.Drawing.Point(474, 35);
            this.btnUnosAuta.Name = "btnUnosAuta";
            this.btnUnosAuta.Size = new System.Drawing.Size(161, 45);
            this.btnUnosAuta.TabIndex = 12;
            this.btnUnosAuta.Text = "Unesi novi auto";
            this.btnUnosAuta.UseVisualStyleBackColor = true;
            this.btnUnosAuta.Click += new System.EventHandler(this.btnUnosAuta_Click);
            // 
            // lblKamioni
            // 
            this.lblKamioni.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblKamioni.Location = new System.Drawing.Point(246, 9);
            this.lblKamioni.Name = "lblKamioni";
            this.lblKamioni.Size = new System.Drawing.Size(198, 23);
            this.lblKamioni.TabIndex = 11;
            this.lblKamioni.Text = "Lista kamiona";
            this.lblKamioni.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAuti
            // 
            this.lblAuti.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblAuti.Location = new System.Drawing.Point(12, 9);
            this.lblAuti.Name = "lblAuti";
            this.lblAuti.Size = new System.Drawing.Size(199, 23);
            this.lblAuti.TabIndex = 10;
            this.lblAuti.Text = "Lista auta";
            this.lblAuti.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxAuti
            // 
            this.listBoxAuti.FormattingEnabled = true;
            this.listBoxAuti.ItemHeight = 15;
            this.listBoxAuti.Location = new System.Drawing.Point(12, 35);
            this.listBoxAuti.Name = "listBoxAuti";
            this.listBoxAuti.Size = new System.Drawing.Size(199, 439);
            this.listBoxAuti.TabIndex = 8;
            // 
            // listBoxKamioni
            // 
            this.listBoxKamioni.FormattingEnabled = true;
            this.listBoxKamioni.ItemHeight = 15;
            this.listBoxKamioni.Location = new System.Drawing.Point(246, 35);
            this.listBoxKamioni.Name = "listBoxKamioni";
            this.listBoxKamioni.Size = new System.Drawing.Size(199, 439);
            this.listBoxKamioni.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(662, 513);
            this.Controls.Add(this.listBoxKamioni);
            this.Controls.Add(this.btnSecretMenu);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnUnosKamion);
            this.Controls.Add(this.btnUnosAuta);
            this.Controls.Add(this.lblKamioni);
            this.Controls.Add(this.lblAuti);
            this.Controls.Add(this.listBoxAuti);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btnSecretMenu;
        private TextBox textBox1;
        private Button btnUnosKamion;
        private Button btnUnosAuta;
        private Label lblKamioni;
        private Label lblAuti;
        private ListBox listBoxAuti;
        private ListBox listBoxKamioni;
    }
}