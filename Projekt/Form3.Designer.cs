﻿namespace Projekt
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnZatvori = new System.Windows.Forms.Button();
            this.txtBoxKilometri = new System.Windows.Forms.TextBox();
            this.lblPrijedeniKilometri = new System.Windows.Forms.Label();
            this.btnUnesi = new System.Windows.Forms.Button();
            this.txtBoxBojaKamiona = new System.Windows.Forms.TextBox();
            this.txtBoxSnagaKamiona = new System.Windows.Forms.TextBox();
            this.txtBoxNazivKamiona = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.lblBojaKamiona = new System.Windows.Forms.Label();
            this.lblNazivKamiona = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnZatvori
            // 
            this.btnZatvori.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnZatvori.Location = new System.Drawing.Point(12, 341);
            this.btnZatvori.Name = "btnZatvori";
            this.btnZatvori.Size = new System.Drawing.Size(213, 33);
            this.btnZatvori.TabIndex = 29;
            this.btnZatvori.Text = "Zatvori porzor";
            this.btnZatvori.UseVisualStyleBackColor = true;
            this.btnZatvori.Click += new System.EventHandler(this.btnZatvori_Click);
            // 
            // txtBoxKilometri
            // 
            this.txtBoxKilometri.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxKilometri.Location = new System.Drawing.Point(12, 252);
            this.txtBoxKilometri.Name = "txtBoxKilometri";
            this.txtBoxKilometri.Size = new System.Drawing.Size(213, 29);
            this.txtBoxKilometri.TabIndex = 28;
            // 
            // lblPrijedeniKilometri
            // 
            this.lblPrijedeniKilometri.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblPrijedeniKilometri.Location = new System.Drawing.Point(12, 226);
            this.lblPrijedeniKilometri.Name = "lblPrijedeniKilometri";
            this.lblPrijedeniKilometri.Size = new System.Drawing.Size(213, 23);
            this.lblPrijedeniKilometri.TabIndex = 27;
            this.lblPrijedeniKilometri.Text = "Prijedeni kilometri";
            this.lblPrijedeniKilometri.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnUnesi
            // 
            this.btnUnesi.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnUnesi.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnUnesi.Location = new System.Drawing.Point(12, 287);
            this.btnUnesi.Name = "btnUnesi";
            this.btnUnesi.Size = new System.Drawing.Size(213, 33);
            this.btnUnesi.TabIndex = 26;
            this.btnUnesi.Text = "Unesi";
            this.btnUnesi.UseVisualStyleBackColor = true;
            this.btnUnesi.Click += new System.EventHandler(this.btnUnesi_Click);
            // 
            // txtBoxBojaKamiona
            // 
            this.txtBoxBojaKamiona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxBojaKamiona.Location = new System.Drawing.Point(12, 104);
            this.txtBoxBojaKamiona.Name = "txtBoxBojaKamiona";
            this.txtBoxBojaKamiona.Size = new System.Drawing.Size(213, 29);
            this.txtBoxBojaKamiona.TabIndex = 25;
            // 
            // txtBoxSnagaKamiona
            // 
            this.txtBoxSnagaKamiona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxSnagaKamiona.Location = new System.Drawing.Point(12, 179);
            this.txtBoxSnagaKamiona.Name = "txtBoxSnagaKamiona";
            this.txtBoxSnagaKamiona.Size = new System.Drawing.Size(213, 29);
            this.txtBoxSnagaKamiona.TabIndex = 24;
            // 
            // txtBoxNazivKamiona
            // 
            this.txtBoxNazivKamiona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.txtBoxNazivKamiona.Location = new System.Drawing.Point(12, 35);
            this.txtBoxNazivKamiona.Name = "txtBoxNazivKamiona";
            this.txtBoxNazivKamiona.Size = new System.Drawing.Size(213, 29);
            this.txtBoxNazivKamiona.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(48, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 23);
            this.label3.TabIndex = 22;
            this.label3.Text = "Snaga kamiona";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblBojaKamiona
            // 
            this.lblBojaKamiona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblBojaKamiona.Location = new System.Drawing.Point(48, 78);
            this.lblBojaKamiona.Name = "lblBojaKamiona";
            this.lblBojaKamiona.Size = new System.Drawing.Size(134, 23);
            this.lblBojaKamiona.TabIndex = 21;
            this.lblBojaKamiona.Text = "Boja kamiona";
            this.lblBojaKamiona.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNazivKamiona
            // 
            this.lblNazivKamiona.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNazivKamiona.Location = new System.Drawing.Point(48, 9);
            this.lblNazivKamiona.Name = "lblNazivKamiona";
            this.lblNazivKamiona.Size = new System.Drawing.Size(134, 23);
            this.lblNazivKamiona.TabIndex = 20;
            this.lblNazivKamiona.Text = "Naziv kamiona";
            this.lblNazivKamiona.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(240, 386);
            this.Controls.Add(this.btnZatvori);
            this.Controls.Add(this.txtBoxKilometri);
            this.Controls.Add(this.lblPrijedeniKilometri);
            this.Controls.Add(this.btnUnesi);
            this.Controls.Add(this.txtBoxBojaKamiona);
            this.Controls.Add(this.txtBoxSnagaKamiona);
            this.Controls.Add(this.txtBoxNazivKamiona);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblBojaKamiona);
            this.Controls.Add(this.lblNazivKamiona);
            this.Name = "Form3";
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btnZatvori;
        private TextBox txtBoxKilometri;
        private Label lblPrijedeniKilometri;
        private Button btnUnesi;
        private TextBox txtBoxBojaKamiona;
        private TextBox txtBoxSnagaKamiona;
        private TextBox txtBoxNazivKamiona;
        private Label label3;
        private Label lblBojaKamiona;
        private Label lblNazivKamiona;
    }
}