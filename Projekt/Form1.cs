namespace Projekt
{
    public partial class Form1 : Form
    {
        public AutoBaza autoBaza;
        public KamionBaza kamionBaza;
        public Form1()
        {
            InitializeComponent();
            autoBaza = new AutoBaza();
            kamionBaza = new KamionBaza();
            listBoxAuti.Items.AddRange(autoBaza.DohvatiAute().ToArray());
            listBoxKamioni.Items.AddRange(kamionBaza.DohvatiKamione().ToArray());
        }

        private void btnUnosAuta_Click(object sender, EventArgs e)
        {
            Form2 formaUnosAuta = new Form2();
            if (formaUnosAuta.ShowDialog() == DialogResult.OK) {
                Auto? noviAuto = formaUnosAuta.NoviAuto;
                if (noviAuto != null) {
                    autoBaza.DodajAuto(noviAuto);
                    listBoxAuti.Items.Clear();
                    listBoxAuti.Items.AddRange(autoBaza.DohvatiAute().ToArray());
                }
            }
        }

        private void btnUnosKamion_Click(object sender, EventArgs e)
        {
            Form3 formaUnosKamiona = new Form3();
            if (formaUnosKamiona.ShowDialog() == DialogResult.OK) {
                Kamion? noviKamion = formaUnosKamiona.NoviKamion;
                if (noviKamion != null) {
                    kamionBaza.DodajKamion(noviKamion);
                    listBoxKamioni.Items.Clear();
                    listBoxKamioni.Items.AddRange(kamionBaza.DohvatiKamione().ToArray());
                }
            }
        }

        private void btnSecretMenu_Click(object sender, EventArgs e)
        {
            Form4 tajnaForma = new Form4();
            tajnaForma.Show();
        }
    }
}