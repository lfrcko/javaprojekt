﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt {
    public partial class Form4 : Form {
        private delegate void Loading();
        private delegate void Delegat(string poruka);
        private event Delegat IspisPoruke;
        public Form4() {
            InitializeComponent();
            Delegat delegat = Ispisi;
            delegat("Radi delegat!");
            Auto auto = new Auto("Zastava", "crvena", 80, 3);
            var smart = new { nazivVoz = "Porsche", bojaVoz = "siva", snagaVoz = "100", brojVrata = "5" };
            MessageBox.Show(smart.ToString());
            if (smart.Equals(new { nazivVoz = "Porsche", bojaVoz = "siva", snagaVoz = "100", brojVrata = "5" })) {
                MessageBox.Show("Auti su isti.");
            } else {
                MessageBox.Show("Auti nisu isti.");
            }
            //auto.IspisPoruke += Ispisi;
            //CPU cpu = new CPU (1523.2);
            //CPU.Procesor procesor = cpu.new Procesor(8, "AMD");
            Vanjska.Unutarnja.Ispis();
        }

        private void Ucitaj() {
            for (int i=0; i <= 100; i++) {
                if (progressBar.InvokeRequired) {
                    progressBar.Invoke(new Action(Ucitaj));
                }
                else {
                    progressBar.Value = i;
                }
                Thread.Sleep(100);
                if (i == 99) {
                    MessageBox.Show("Podatak ucitan");
                    break;
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e) {
            Thread t1 = new Thread(Ucitaj);
            t1.Start();
            /*
            Loading load = Ucitaj;
            load.BeginInvoke(new AsyncCallback(Ucitano), null);
            */
        }

        private void Ucitano(IAsyncResult iar) {
            MessageBox.Show("Podatak ucitan");
        }

        private void Ispisi(string poruka) {
            if (IspisPoruke != null) {
                MessageBox.Show("Ispis poruke");
            }
            MessageBox.Show("Poruka: {0}", poruka);
        }
    }
}
