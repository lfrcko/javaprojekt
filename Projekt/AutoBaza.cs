﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace Projekt
{
    public class AutoBaza
    {
        private List<Auto> bazaAuta;

        public AutoBaza()
        {
            bazaAuta = new List<Auto>();
            Deserialize();
        }

        public void DodajAuto(Auto auto)
        {
            bazaAuta.Add(auto);
            Serialize();
        }

        public IEnumerable<Auto> DohvatiAute()
        {
            return
              from a in bazaAuta
              orderby a.nazivVoz
              select a;
        }

        public void Serialize()
        {
            try
            {
                string autiInJson = JsonSerializer.Serialize(bazaAuta);
                using (StreamWriter AutiUnos = new(@"D:\Projekt\Projekt\Auti.txt"))
                {
                    AutiUnos.WriteLine(autiInJson);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Nema podataka");
                Console.WriteLine(ex.Message);
            }
        }

        public void Deserialize()
        {
            try
            {
                string json = File.ReadAllText(@"D:\Projekt\Projekt\Auti.txt");
                bazaAuta = JsonSerializer.Deserialize<List<Auto>>(json)!;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
